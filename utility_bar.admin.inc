<?php
// $Id$

/**
 * @file utility_bar.admin.inc
 * This include file contains admin-related callback and form functions, primarily
 * to allow admin system/config settings.
 */


/**
 * Main system settings form for Utility Bar module.
 */ 
function utility_bar_admin() {
  $form = array();
  
  $form['utility_bar_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display utility bar for logged in users'),
    '#default_value' => variable_get('utility_bar_display', UTILITY_BAR_DISPLAY),
    '#description' => t('Check this option to display the utility bar for authorized users. Unchecking this will effectively disable the utility bar.'),
  );
  
  $form['utility_bar_display_anon'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display utility bar for anonymous users'),
    '#default_value' => variable_get('utility_bar_display_anon', UTILITY_BAR_DISPLAY_ANON),
    '#description' => t('Check this option to display the utility bar for users who are not logged in. <strong>Note</strong>: It may take several minutes for the utility bar to show up due to Akamai page caching.'),
  );
  
  $form['utility_bar_messages_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Move system messages to utility bar	'),
    '#default_value' => variable_get('utility_bar_messages_display', FALSE),
    '#description' => t('Check this option to move Drupal system messages to the utility bar from the usual content location.'),
  );
  
  $form['utility_bar_messages_header'] = array(
    '#type' => 'textfield',
    '#title' => t('System messages header'),
    '#default_value' => variable_get('utility_bar_messages_header', 'System Messages'),
    '#description' => t('Header to display for Drupal system messages.'),
  );

  return system_settings_form($form);
}


function utility_bar_admin_blocks() {
  $theme = variable_get('theme_default', 'garland');
  $block_results = db_query("SELECT * FROM {blocks} WHERE theme='%s'", $theme);
  $block_options = array();
  while ($block_data = db_fetch_array($block_results)) {
    $block = module_invoke($block_data['module'], 'block', 'list', $block_data['delta']);
    $block_options["{$block_data['module']}--{$block_data['delta']}"] = $block_data['module'] . ': ' . $block[$block_data['delta']]['info'];
  }

  $form = array();
  
  // Sort values.
  asort($block_options);
  
  $form['utility_bar_blocks'] = array(
    '#title' => t('Blocks'),
    '#description' => t('Select the blocks to appear in the Utility Bar.'),
    '#type' => 'checkboxes',
    '#default_value' => variable_get('utility_bar_blocks', array()),
    '#options' => $block_options,
  );

  return system_settings_form($form);
}
