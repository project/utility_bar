<?php $messages = 0; ?>
<div id="utility-bar-wrapper">
  <div id="utility-bar-content">

    <div id="utility-bar-user-links">
      <?php print $login_block; ?>
    </div>
    
    <div id="utility-bar-block-links">
      <?php foreach($blocks as $block): ?>
      <?php $block_delta ++; ?>
      <div class="utility-bar-user-content">
        <div class="utility-bar-user-link" rel="utility-bar-user-block-<?php print $block_delta; ?>"><?php print l($block['subject'], ''); //TODO: Make this link to a callback page that displays the desired block. ?></div>
        <div class="utility-bar-user-block" id="utility-bar-user-block-<?php print $block_delta; ?>">
          <div class="top"><div></div></div>
          <div class="block-content">
            <div class="content-inside clear-block">
              <div class="close-wrapper"><div class="close"><a href="#" rel="utility-bar-user-block-<?php print $block_delta; ?>"><?php print t('Close') ?></a></div></div>
              <h4><?php print $block['subject']; ?></h4>
              <div><?php print $block['content']; ?></div>
            </div>
          </div>
          <div class="bottom"><div>&nbsp;</div></div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>

    <div class="view-messages utility-messages messages-<?php print $messages; ?>">
      <?php print l(t('View !messages', array('!messages' => variable_get('wh_utility_bar_messages_header', 'System Messages'))), 'view-messages') ?>
      <div id="utility-bar-messages">
        <div class="top"><div></div></div>
        <div class="content">
          <div class="content-inside">
            <h4><?php print variable_get('wh_utility_bar_messages_header', 'System Messages'); ?></h4>
            <div class="close-wrapper"><div class="close"><a href="#"><?php print t('Close') ?></a></div></div>
          </div>
        </div>
        <div class="bottom"><div>&nbsp;</div></div>
      </div>
      <?php $messages ++; ?>
    </div>

  </div>
</div>