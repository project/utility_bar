function utility_bar_display() {
  return true;
}

Drupal.behaviors.utility_bar_discussion_toolbar = function (context) {
  $(".utility-bar-user-link a", context).click(function () {
    // Get the ID of the target block
    var id = '#' + $(this).parent().attr('rel');

    // Hide all other possibly open blocks
    $(".utility-bar-user-block:not(" + id + ")").hide();
    
    // Show/hide the user-selected block
    $(id).slideToggle('fast');
    return false;
  });
  
  $(".utility-bar-user-block .close a", context).click(function (context) {
    var id = '#' + $(this).attr('rel');
    $(id).slideUp('fast'); // slideUp will actually slide down (slideUp == close)
    return false;
  });
};

Drupal.behaviors.utility_bar_display = function (context) {
  if (utility_bar_display()) {
    $("#utility-bar-wrapper").show();
  }
  else {
    $("#utility-bar-wrapper").hide();
  }
}

Drupal.behaviors.utility_bar_messages = function (context) {
  if (utility_bar_display() && Drupal.settings.utility_bar.utility_bar_messages_display) {
    if ($(".messages", context).length) {

      $(".messages", context).clone().appendTo("#utility-bar-messages .content .content-inside");
      setTimeout(function(context) { $('#utility-bar-messages').slideToggle('fast'); }, 600);
      setTimeout(function(context) { $('#utility-bar-messages').slideUp('fast'); }, 4600);

      $('.messages', context).toggle();
      $("#utility-bar-messages .messages").toggle();
      $('.view-messages', context).toggle();
  
      $("#utility-bar-messages .close a", context).click(function () {
        $('#utility-bar-messages').slideDown('fast');
        return false;
      });
      
      $("#utility-bar-content .view-messages a", context).click(function () {
        $('#utility-bar-messages').slideToggle('fast');
        return false;
      });
    }
  }
};

